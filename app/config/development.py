# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from app.config.basic import BasicConfig


class DevelopmentConfig(BasicConfig):
    """
    Development configuration
    """
    DEBUG = True
    SQLALCHEMY_ECHO = True
