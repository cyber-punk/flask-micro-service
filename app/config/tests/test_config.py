# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import unittest
from mock import patch, MagicMock

from app.config import CONFIG_DEVELOPMENT, CONFIG_TESTING, CONFIG_PRODUCTION, \
    Config, DevelopmentConfig, TestingConfig, ProductionConfig


class TestConfig(unittest.TestCase):
    """
    Test configuration's factory
    """
    CONFIG_NAMES = [
        CONFIG_DEVELOPMENT,
        CONFIG_TESTING,
        CONFIG_PRODUCTION
    ]

    def test_default_is_development(self):
        self.assertIsInstance(
            Config(),
            DevelopmentConfig
        )

    def test_development_config(self):
        self.assertIsInstance(
            Config(name=CONFIG_DEVELOPMENT),
            DevelopmentConfig
        )

    def test_testing_config(self):
        self.assertIsInstance(
            Config(name=CONFIG_TESTING),
            TestingConfig
        )

    def test_production_config(self):
        self.assertIsInstance(
            Config(name=CONFIG_PRODUCTION),
            ProductionConfig
        )

    def test_constructor(self):
        """
        Create with application parameter,
        check that init_app method was called
        """
        my_app = MagicMock()
        for config_name in self.CONFIG_NAMES:
            with patch('app.config.basic.BasicConfig.init_app') as mocked:
                Config(my_app, name=config_name)
                mocked.assert_called_once_with(my_app)

    def test_init_app(self):
        """
        Call init_app method with application parameter,
        check that app.config.from_object method was called
        """
        my_app = MagicMock()
        for config_name in self.CONFIG_NAMES:
            with patch.object(my_app.config, 'from_object') as mocked:
                config = Config(name=config_name)
                config.init_app(my_app)
                mocked.assert_called_once_with(config.__class__)


if __name__ == '__main__':
    unittest.main()
