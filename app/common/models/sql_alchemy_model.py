# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from sqlalchemy.ext.declarative import declared_attr, has_inherited_table

from app.database.sql_alchemy import db
from app.common.models.base_model import BaseModel
from app.common.utils import decapitalize, plural


class SQLAlchemyModel(BaseModel, db.Model):
    """
    Base abstract model class for sql alchemy engine
    """
    __abstract__ = True

    id = db.Column(db.Integer, primary_key=True)

    @declared_attr
    def __tablename__(self):
        """
        Convert class-name to table-name.
        For example:
            MyModel => my_models
        """
        if not has_inherited_table(self):
            return plural(decapitalize(self.__name__))
