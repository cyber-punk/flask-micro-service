# -*- coding: utf-8 -*-
# pylint: disable=no-member
from __future__ import unicode_literals

import json
import unittest

from flask import url_for
from marshmallow import fields, ValidationError

from app.tests import TestCase
from app.database.sql_alchemy import db
from app.common.api import BaseApi, BaseResource
from app.common.serializers import BaseSerializer
from app.common.models import SQLAlchemyModel
from app.common.services import SQLAlchemyStorage


def can_not_be_blank(data):
    if not data:
        raise ValidationError('Required field can not be blank.')


class MySerializer(BaseSerializer):
    my_field = fields.String(required=True, validate=can_not_be_blank)


class MyModel(SQLAlchemyModel):
    __bind_key__ = 'test'
    my_field = db.Column(db.String(80), unique=True)


class MyStorage(SQLAlchemyStorage):
    MODEL = MyModel


class MyResource(BaseResource):
    SERIALIZER = MySerializer
    STORAGE = MyStorage


class MyApi(BaseApi):
    PREFIX = '/api/test'
    API_RESOURCES = [
        (MyResource, ('/my', '/my/<int:id>')),
    ]


TEST_STRING_1 = 'Some folks for certain have thought it was shocking,'
TEST_STRING_2 = 'When Famine appeals and when Poverty groans,'
TEST_STRING_3 = 'That Life should be valued at less than a stocking,'
TEST_STRING_4 = 'And breaking of frames lead to breaking of bones.'


class TestBaseApi(TestCase):

    @classmethod
    def setUpClass(cls):
        super(TestBaseApi, cls).setUpClass()
        cls.app.my_api = MyApi(app=cls.app)

    def setUp(self):
        for my_field in (TEST_STRING_1, TEST_STRING_2, TEST_STRING_3):
            self.client.post(
                url_for('api_test__my_resource'),
                content_type='application/json',
                data=json.dumps({
                    'my_field': my_field
                }))

    def tearDown(self):
        for i in (1, 2, 3, 4):
            self.client.delete(
                url_for('api_test__my_resource', id=i))

    def test_get_one(self):
        response = self.client.get(
            url_for('api_test__my_resource', id=1))
        self.assertEqual(200, response.status_code)
        self.assertEqual('application/json', response.content_type)
        self.assertEqual({
            'id': 1,
            'my_field': TEST_STRING_1
        }, json.loads(response.data))

    def test_get_not_found(self):
        response = self.client.get(
            url_for('api_test__my_resource', id=4))
        self.assertEqual(404, response.status_code)
        self.assertEqual('application/json', response.content_type)
        self.assertEqual({
            'error': 'Resource does not exist'
        }, json.loads(response.data))

    def test_get_many(self):
        response = self.client.get(
            url_for('api_test__my_resource'))
        self.assertEqual(200, response.status_code)
        self.assertEqual('application/json', response.content_type)
        self.assertEqual({
            'filter': {},
            'paging': {'limit': 1024, 'offset': 0, 'total': 3},
            'data': [
                {'id': 1, 'my_field': TEST_STRING_1},
                {'id': 2, 'my_field': TEST_STRING_2},
                {'id': 3, 'my_field': TEST_STRING_3}
            ]
        }, json.loads(response.data))

    def test_get_many_paging(self):
        response = self.client.get(
            url_for('api_test__my_resource'),
            query_string={'limit': 2, 'offset': 2})
        self.assertEqual(200, response.status_code)
        self.assertEqual('application/json', response.content_type)
        self.assertEqual({
            'filter': {},
            'paging': {'limit': 2, 'offset': 2, 'total': 3},
            'data': [
                {'id': 3, 'my_field': TEST_STRING_3}
            ]
        }, json.loads(response.data))

    def test_get_bad_request(self):
        response = self.client.get(
            url_for('api_test__my_resource'),
            query_string={'limit': -1, 'offset': -1})
        self.assertEqual(400, response.status_code)
        self.assertEqual('application/json', response.content_type)
        self.assertEqual({
            'paging': {
                'limit': ['Must be between 1 and 1024.'],
                'offset': ['Must be at least 0.']
            }
        }, json.loads(response.data))

    def test_create(self):
        response = self.client.post(
            url_for('api_test__my_resource'),
            content_type='application/json',
            data=json.dumps({
                'my_field': TEST_STRING_4
            }))
        self.assertEqual(201, response.status_code)
        self.assertEqual('application/json', response.content_type)
        self.assertEqual({
            'id': 4,
            'my_field': TEST_STRING_4
        }, json.loads(response.data))

    def test_create_bad_request(self):
        response = self.client.post(
            url_for('api_test__my_resource'),
            content_type='application/json',
            data=json.dumps({}))
        self.assertEqual(400, response.status_code)
        self.assertEqual('application/json', response.content_type)
        self.assertEqual({
            'my_field': ['Missing data for required field.']
        }, json.loads(response.data))

    def test_create_not_json(self):
        response = self.client.post(
            url_for('api_test__my_resource'),
            content_type='application/json',
            data={
                'my_field': TEST_STRING_4
            })
        self.assertEqual(400, response.status_code)
        self.assertEqual('application/json', response.content_type)
        self.assertEqual({
            'error': 'The browser (or proxy) sent a request that '
                     'this server could not understand.'
        }, json.loads(response.data))

    def test_put(self):
        response = self.client.put(
            url_for('api_test__my_resource', id=1),
            content_type='application/json',
            data=json.dumps({
                'id': 1,
                'my_field': TEST_STRING_4
            }))
        self.assertEqual(200, response.status_code)
        self.assertEqual('application/json', response.content_type)
        self.assertEqual({
            'id': 1,
            'my_field': TEST_STRING_4
        }, json.loads(response.data))

    def test_put_not_found(self):
        response = self.client.put(
            url_for('api_test__my_resource', id=4),
            content_type='application/json',
            data=json.dumps({
                'id': 4,
                'my_field': TEST_STRING_4
            }))
        self.assertEqual(404, response.status_code)
        self.assertEqual('application/json', response.content_type)
        self.assertEqual({
            'error': 'Resource does not exist'
        }, json.loads(response.data))

    def test_put_already_exists(self):
        response = self.client.put(
            url_for('api_test__my_resource', id=1),
            content_type='application/json',
            data=json.dumps({
                'id': 1,
                'my_field': TEST_STRING_2
            }))
        self.assertEqual(409, response.status_code)
        self.assertEqual('application/json', response.content_type)
        self.assertEqual({
            'error': 'Resource already exists'
        }, json.loads(response.data))

    def test_put_bad_request(self):
        response = self.client.put(
            url_for('api_test__my_resource', id=1),
            content_type='application/json',
            data=json.dumps({
                'id': 1
            }))
        self.assertEqual(400, response.status_code)
        self.assertEqual('application/json', response.content_type)
        self.assertEqual({
            'my_field': ['Missing data for required field.']
        }, json.loads(response.data))

    def test_put_not_json(self):
        response = self.client.put(
            url_for('api_test__my_resource', id=1),
            content_type='application/json',
            data={
                'id': 1,
                'my_field': TEST_STRING_4
            })
        self.assertEqual(400, response.status_code)
        self.assertEqual('application/json', response.content_type)
        self.assertEqual({
            'error': 'The browser (or proxy) sent a request that '
                     'this server could not understand.'
        }, json.loads(response.data))

    def test_patch(self):
        response = self.client.patch(
            url_for('api_test__my_resource', id=1),
            content_type='application/json',
            data=json.dumps({
                'my_field': TEST_STRING_4
            }))
        self.assertEqual(200, response.status_code)
        self.assertEqual('application/json', response.content_type)
        self.assertEqual({
            'id': 1,
            'my_field': TEST_STRING_4
        }, json.loads(response.data))

    def test_patch_not_found(self):
        response = self.client.patch(
            url_for('api_test__my_resource', id=4),
            content_type='application/json',
            data=json.dumps({
                'my_field': TEST_STRING_4
            }))
        self.assertEqual(404, response.status_code)
        self.assertEqual('application/json', response.content_type)
        self.assertEqual({
            'error': 'Resource does not exist'
        }, json.loads(response.data))

    def test_patch_already_exists(self):
        response = self.client.patch(
            url_for('api_test__my_resource', id=1),
            content_type='application/json',
            data=json.dumps({
                'my_field': TEST_STRING_2
            }))
        self.assertEqual(409, response.status_code)
        self.assertEqual('application/json', response.content_type)
        self.assertEqual({
            'error': 'Resource already exists'
        }, json.loads(response.data))

    def test_patch_bad_request(self):
        response = self.client.patch(
            url_for('api_test__my_resource', id=1),
            content_type='application/json',
            data=json.dumps({
                'my_field': ''
            }))
        self.assertEqual(400, response.status_code)
        self.assertEqual('application/json', response.content_type)
        self.assertEqual({
            'my_field': ['Required field can not be blank.']
        }, json.loads(response.data))

    def test_patch_not_json(self):
        response = self.client.patch(
            url_for('api_test__my_resource', id=1),
            content_type='application/json',
            data={
                'my_field': TEST_STRING_4
            })
        self.assertEqual(400, response.status_code)
        self.assertEqual('application/json', response.content_type)
        self.assertEqual({
            'error': 'The browser (or proxy) sent a request that '
                     'this server could not understand.'
        }, json.loads(response.data))

    def test_delete(self):
        response = self.client.delete(
            url_for('api_test__my_resource', id=1))
        self.assertEqual(204, response.status_code)
        self.assertEqual('application/json', response.content_type)
        self.assertEqual('', response.data)

    def test_delete_not_found(self):
        response = self.client.delete(
            url_for('api_test__my_resource', id=4))
        self.assertEqual(404, response.status_code)
        self.assertEqual('application/json', response.content_type)
        self.assertEqual({
            'error': 'Resource does not exist'
        }, json.loads(response.data))


if __name__ == '__main__':
    unittest.main()
