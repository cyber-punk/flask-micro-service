# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from app.common.models import BaseModel


class BaseStorage(object):
    MODEL = BaseModel

    def __init__(self):
        super(BaseStorage, self).__init__()
        self.MODEL = self.__class__.MODEL
        if not issubclass(self.MODEL, BaseModel):
            raise TypeError('Not a model:', self.MODEL)

    def commit(self):
        pass

    def rollback(self):
        raise NotImplementedError('Can not rollback')

    def count(self, filter=None):
        return self._get_count(filter=filter)

    def read(self, id=None, filter=None, paging=None):
        if id is not None:
            return self._get_item(id)
        else:
            return self._get_list(filter=filter, paging=paging)

    def _get_item(self, id):
        raise NotImplementedError

    def _get_list(self, filter=None, paging=None):
        raise NotImplementedError

    def _get_count(self, filter=None):
        raise NotImplementedError

    def create(self, **attrs):
        model = self.MODEL(**attrs)
        self._save(model)
        return model

    def update(self, **attrs):
        model = self._get_item(id=attrs['id'])
        for attr_name, attr_value in attrs.items():
            if hasattr(model, attr_name):
                setattr(model, attr_name, attr_value)
        self._save(model)
        return model

    def _save(self, model):
        raise NotImplementedError

    def delete(self, id):
        model = self._get_item(id)
        self._remove(model)

    def _remove(self, model):
        raise NotImplementedError
