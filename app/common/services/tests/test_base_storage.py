# -*- coding: utf-8 -*-
# pylint: disable=abstract-method
# pylint: disable=protected-access
from __future__ import unicode_literals

import unittest
from mock import patch, MagicMock

from app.common.utils import patch_multiple, AssertsMixin
from app.common.models import BaseModel
from app.common.services import BaseStorage


class MyModel(BaseModel):
    my_field = None


class MyStorage(BaseStorage):
    MODEL = MyModel


TEST_STR_1 = 'That life should be valued at less than a stocking'
TEST_STR_2 = 'And breaking of frames lead to breaking of bones'


class TestBaseStorage(AssertsMixin, unittest.TestCase):
    """
    Test BaseStorage in isolation mode
    """

    def test_has_attr_commit(self):
        self.assertTrue(hasattr(BaseStorage, 'commit'))

    def test_has_attr_rollback(self):
        self.assertTrue(hasattr(BaseStorage, 'rollback'))

    def test_has_attr_create(self):
        self.assertTrue(hasattr(BaseStorage, 'create'))

    def test_has_attr_read(self):
        self.assertTrue(hasattr(BaseStorage, 'read'))

    def test_has_attr_update(self):
        self.assertTrue(hasattr(BaseStorage, 'update'))

    def test_has_attr_delete(self):
        self.assertTrue(hasattr(BaseStorage, 'delete'))

    def test_model_type_validation(self):

        class BadModel(object):
            pass

        class BadStorage(BaseStorage):
            MODEL = BadModel

        with self.assertRaisesDetails(TypeError, BadStorage) as e:
            self.assertEqual('Not a model:', e.args[0])
            self.assertEqual(BadModel, e.args[1])

    def test_count(self):
        with patch('app.common.services.base_storage'
                   '.BaseStorage._get_count') as mocked:
            mocked.return_value = 3
            result = MyStorage().count()
            mocked.assert_called_with(filter=None)
            self.assertEqual(result, 3)

    def test_read_one(self):
        with patch('app.common.services.base_storage'
                   '.BaseStorage._get_item') as mocked:
            model_item = mocked.return_value = MyModel(
                id=1, my_field=TEST_STR_1)
            result = MyStorage().read(1)
            mocked.assert_called_with(1)
            self.assertEqual(result, model_item)

    def test_read_many(self):
        with patch('app.common.services.base_storage'
                   '.BaseStorage._get_list') as mocked:
            model_list = mocked.return_value = [
                MyModel(id=1, my_field=TEST_STR_1),
                MyModel(id=2, my_field=TEST_STR_2),
            ]
            result = MyStorage().read()
            mocked.assert_called_with(filter=None, paging=None)
            self.assertEqual(result, model_list)

    def test_create(self):
        with patch('app.common.services.base_storage'
                   '.BaseStorage._save') as mocked:
            model_item = mocked.return_value = MyModel(
                id=1, my_field=TEST_STR_1)
            result = MyStorage().create(my_field=TEST_STR_1)
            mocked.assert_called_with(
                MyModel(my_field=TEST_STR_1)
            )
            self.assertEqual(result, model_item)

    def test_update(self):
        with patch_multiple([
            'app.common.services.base_storage.BaseStorage._get_item',
            'app.common.services.base_storage.BaseStorage._save'
        ]) as (m_get_item, m_save):
            model_item = m_get_item.return_value = MyModel(
                id=1, my_field=TEST_STR_1)
            result = MyStorage().update(
                id=1, my_field=TEST_STR_2)
            m_get_item.assert_called_with(id=1)
            self.assertEqual(1, model_item.id)
            self.assertEqual(TEST_STR_2, model_item.my_field)
            m_save.assert_called_with(model_item)
            self.assertEqual(result, model_item)

    def test_delete(self):
        with patch_multiple([
            'app.common.services.base_storage.BaseStorage._get_item',
            'app.common.services.base_storage.BaseStorage._remove'
        ]) as (m_get_item, m_remove):
            model_item = m_get_item.return_value = MagicMock()
            result = MyStorage().delete(1)
            m_get_item.assert_called_with(1)
            m_remove.assert_called_with(model_item)
            self.assertIsNone(result)

    def test_commit_is_abstract(self):
        self.assertIsNone(MyStorage().commit())

    def test_rollback_is_abstract(self):
        with self.assertRaisesDetails(
            NotImplementedError,
            MyStorage().rollback
        ) as e:
            self.assertEqual(e.args[0], 'Can not rollback')

    def test_count_is_abstract(self):
        with self.assertRaises(NotImplementedError):
            MyStorage()._get_count()

    def test_get_item_is_abstract(self):
        with self.assertRaises(NotImplementedError):
            MyStorage()._get_item(1)

    def test_get_list_is_abstract(self):
        with self.assertRaises(NotImplementedError):
            MyStorage()._get_list()

    def test_save_is_abstract(self):
        with self.assertRaises(NotImplementedError):
            MyStorage()._save(model=MagicMock())

    def test_remove_is_abstract(self):
        with self.assertRaises(NotImplementedError):
            MyStorage()._remove(model=MagicMock())


if __name__ == '__main__':
    unittest.main()
