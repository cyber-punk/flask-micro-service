# -*- coding: utf-8 -*-
from __future__ import unicode_literals


class _AssertRaisesDetailsContext(object):

    def __init__(self, excClass, callableObj=None, *args, **kwargs):
        super(_AssertRaisesDetailsContext, self).__init__()
        try:
            if callableObj:
                callableObj(*args, **kwargs)
        except excClass as e:
            self.exc = e
        else:
            assert False, '{0} not raised'.format(excClass.__name__)

    def __enter__(self):
        return self.exc

    def __exit__(self, exc_type, *args):
        return exc_type is None


class AssertsMixin(object):
    assertRaisesDetails = _AssertRaisesDetailsContext
