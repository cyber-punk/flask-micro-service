# -*- coding: utf-8 -*-
from __future__ import unicode_literals


class Singleton(object):
    """
    Singleton class decorator.
    Realize singleton programming pattern.
    Example of usages:

        @singleton
        class MyClass(object):
            pass
    """
    _instances = {}
    _class = None

    def __init__(self, target):
        """
        This method is called on decorate class

        :param target: the class that we decorate
        """
        self._class = target

    def __call__(self, *args, **kwargs):
        """
        This method is called on class constructor call.
        Create new instance of class if it has not yet existed.

        :param args: not named arguments for class constructor
        :param kwargs: named arguments for class constructor
        :return: instance of class
        """
        if self._class not in self._instances.keys():
            self._instances[self._class] = self._class(*args, **kwargs)
        return self._instances[self._class]
