# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from app.database.sql_alchemy import db as db_sql_alchemy


class Database(object):

    def __init__(self, app=None):
        super(Database, self).__init__()
        self.sql_alchemy = db_sql_alchemy
        if app is not None:
            self.init_app(app)

    def init_app(self, app):
        self.sql_alchemy.init_app(app)
