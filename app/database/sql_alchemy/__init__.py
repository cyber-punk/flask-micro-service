# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os

from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate


class Database(SQLAlchemy):
    """
    Database class with migrations.
    """

    def __init__(self, app=None, **options):
        """
        Constructor.

        :param app: Instance of Flask application, by default is None
        :param **options: are forwarded to the base class constructor
        """
        self.migrations = Migrate(
            db=self, directory=self._get_migrations_directory())
        super(Database, self).__init__(app=app, **options)

    def init_app(self, app):
        """
        Creates tables, migrations.

        :param app: Flask application instance
        """
        super(Database, self).init_app(app)
        self.create_all(app=app)
        self.migrations.init_app(app=app)

    @staticmethod
    def _get_migrations_directory():
        return os.path.abspath(os.path.join(
            os.path.dirname(__file__),
            'migrations'
        ))


db = Database()
