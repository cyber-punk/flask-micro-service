# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from flask import Flask
from flask.config import ConfigAttribute

from app.config import Config
from app.database import Database


class Application(Flask):
    """
    Main application class
    """

    host = ConfigAttribute('HOST')
    port = ConfigAttribute('PORT')

    def __init__(self, config_name=None, **options):
        """
        Constructor.

        :param config_name: is forwarded to the configure method
        :param options: are forwarded to the configure method
        """
        super(Application, self).__init__(__name__)
        self.configure(config_name, **options)
        self.db = Database(app=self)

    def configure(self, config_name=None, **options):
        """
        Update application configuration
        For example:
            app = Application()
            app.configure(host='127.0.0.1', debug=True)

        :param config_name: configuration name
        :param options: are used for update application config.
        """
        self.settings = Config(app=self, name=config_name)
        for attr_name, attr_value in options.items():
            self.config[attr_name.upper()] = attr_value

    def run(self, host=None, port=None, debug=None, **options):
        """
        Run application on a local development server.
        All parameters are forwarded to the parent Flask.run method.
        """
        host = host or self.host
        port = port or self.port
        debug = debug if debug is not None else self.debug
        super(Application, self).run(host, port, debug, **options)
