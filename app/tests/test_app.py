# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import random
import unittest
from mock import patch

from flask import current_app
from flask_sqlalchemy import SQLAlchemy

from app import Application
from app.tests import TestCase


class TestApp(TestCase):
    """
    Simple positive test of Application class
    """

    def test_app_is_valid(self):
        """
        Check that Application.__init__ works.
        """
        self.assertIsInstance(self.app, Application)

    def test_current_app_is_app(self):
        """
        Check that flask.current_app is our application instance.
        """
        self.assertEqual(current_app, self.app)

    def test_app_is_testing(self):
        """
        Check that application instance was created with testing flag.
        """
        self.assertTrue(self.app.testing)

    def test_configure_by_name(self):
        """
        Call with random config_name and
        check that config factory was called with the same argument.
        """
        with patch('app.Config') as mocked:
            self.app.configure(config_name='lorem_ipsum_config')
            mocked.assert_called_once_with(app=self.app,
                                           name='lorem_ipsum_config')

    def test_configure_options(self):
        """
        Call with random arguments and
        check that they were added to the application config.
        """
        options = dict(
            host='.'.join([str(random.randint(0, 255)) for _ in range(4)]),
            port=random.randint(1, 65535),
            debug=bool(random.randint(0, 1)),
            my_param='Lorem ipsum dolor sit amet, tibique phaedrum facilisis.')
        self.app.configure(**options)
        for param, should_be in options.items():
            value = self.app.config.get(param.upper(), None)
            self.assertEqual(value, should_be)

    def test_db_is_correct(self):
        """
        Check that application db contains SQLAlchemy instance.
        """
        self.assertTrue(isinstance(self.app.db.sql_alchemy, SQLAlchemy))

    def test_run_with_custom_params(self):
        """
        Call with random arguments and
        check that they are forwarded to the parent method.
        """
        with patch('flask.Flask.run') as mocked:
            host = '.'.join([str(random.randint(0, 255)) for _ in range(4)])
            port = random.randint(1, 65535)
            debug = bool(random.randint(0, 1))
            options = dict(processes=random.randint(1, 10))
            self.app.run(host, port, debug, **options)
            mocked.assert_called_once_with(host, port, debug, **options)

    def test_run_with_default_params(self):
        """
        Call with default arguments and
        check that application attributes are forwarded to the parent method.
        """
        with patch('flask.Flask.run') as mocked:
            self.app.run()
            mocked.assert_called_once_with(
                self.app.host,
                self.app.port,
                self.app.debug
            )


if __name__ == '__main__':
    unittest.main()
